---
title:  "Raspberry Pi 3 Model B"
image:  "rasp3"
ref: "resource"
categories: "resources.md"
id: 5
---

The Raspberry Pi 3 Model B is the earliest model of the third-generation
Raspberry Pi. It replaced the Raspberry Pi 2 Model B in February 2016. See also
the Raspberry Pi 3 Model B+, the latest product in the Raspberry Pi 3 range.

### URLs

* [Official Site](https://www.raspberrypi.org/products/raspberry-pi-3-model-b/)
