---
title:  "Gold"
image:  "gold"
ref: "gold"
categories: "patrocinio.md"
id: 1
---

# Valor: R$500

# Benefícios

* Identificação da marca patrocinadora em todo material de divulgação do evento: online e offline.
* Logo no Crachá.
* Agradecimento e reconhecimento do patrocinador em todas as atividades relacionadas ao evento.
